# robot

#### 介绍
qq 群通知机器人

#### 软件架构
软件架构说明 
使用 mongodb+Spring boot 2.6.3 代码jdk 1.8+

#### 安装教程

1. 服务器部署go-cqhttp中间组件，官网有教程
2. java -jar demo.jar

#### 使用说明

剑网三丐丐机器人
1.Go_CqHttpSocket 类中tcp 地址改为go-cqhttp 服务得tcp地址
2.application.properties 配置文件，相信java 得朋友们因该熟悉，不做解释。
3.mongodb数据库 创建集合 GroupXbb ， jx3Server 。GroupXbb小本本记录表，jx3Server群绑定服务器表
注：关于go-cqhttp 组件中心 https://docs.go-cqhttp.org/ 部署即可

#### 参与贡献

1.  文档api https://www.jx3api.com/
2.  go-cqhttp 中间组件


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
