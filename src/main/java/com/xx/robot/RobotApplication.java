package com.xx.robot;

import com.xx.robot.common.util.SpringContextUtil;
import com.xx.robot.thrid.PigImageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class RobotApplication {

    @Autowired
    private PigImageUtil pigImageUtil;
    @Autowired
    private SpringContextUtil springContextUtil;

    public static void main(String[] args) {
        SpringApplication.run(RobotApplication.class, args);
    }

    @PostConstruct
    public void init() {
        log.info("数据初始化start");
        springContextUtil.init();
        pigImageUtil.init();
        log.info("数据初始化end");
    }

}
