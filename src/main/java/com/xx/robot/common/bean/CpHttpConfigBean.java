package com.xx.robot.common.bean;

import cn.hutool.setting.Setting;

public class CpHttpConfigBean {

    private static final Setting CP_HTTP_SETTING = new Setting("cpHttp.setting");

    public static String ACTIVE_PROFILES = CP_HTTP_SETTING.get("active");
    public static String MESSAGE_HTTPS = CP_HTTP_SETTING.get("messageHttps");
    public static String WS_TCP = CP_HTTP_SETTING.get("tcp");
    public static String NO_OPEN = CP_HTTP_SETTING.get("noOpen");
    public static String MESSAGE_AT_QQ = CP_HTTP_SETTING.get("messageAtQq");
    public static String ROBOT_NAME = CP_HTTP_SETTING.get("robotName");

}
