package com.xx.robot.common.constant;

/**
 * Mongodb CollectionName
 *
 * @author wiley
 */
public enum CollectionNameEnum {

    // -----------------------j3相关-----------------------------------
    JX3_SERVER("jx3_server", "群绑定服务器"),
    JX3_ACHIEVEMENT_STRATEGY("jx3_achievement_strategy", "成就-奇遇"),
    JX3_SERVE_NEWS("jx3_news", "官网新闻公告"),
    JX3_BOX_UID("jx3_box_uid", "魔盒uid"),

    JX3_PERSONAL_RANK("jx3_personal_RANK", "黑鬼、欧皇榜单"),
    JX3_BLACK_LIST_TEAM("jx3_black_list_team", "团名黑名单"),

    // -----------------------疫情-丁香园数据源----------------------------------
    COVID19_DXY("covid19_dxy", "疫情概览"),
    COVID19_DXY_OVERALL("covid19_dxy_overall", "疫情概览"),
    COVID19_DXY_AREA_NAME("covid19_dxy_area_name", "数据源中区域名称"),
    COVID19_DXY_DATA("covid19_dxy_data", "具体地区数据"),
    COVID19_DXY_NEWS("covid19_dxy_news", "疫情相关新闻"),
    COVID19_DXY_RUMORS("covid19_dxy_rumors", "疫情相关谣言"),

    // -----------------------热搜，排行榜----------------------------------
    HOT_WEIBO("hot_Weibo", "微博热搜"),
    HOT_POPULAR_BILIBILI("hot_popular_Bilibili", "B站综合热门"),
    HOT_RANKING_BILIBILI("hot_ranking_Bilibili", "B站排行榜"),

    // -----------------------其他----------------------------------
    OTHER_JIJIAN_ANJIAN("other_jijian_anjian", "极简按键密码");
    // -----------------------热搜，排行榜----------------------------------

    private String code;
    private String name;

    CollectionNameEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
