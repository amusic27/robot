package com.xx.robot.common.constant;

/**
 * jx3box魔盒url
 */
public interface Jx3BoxUrlEnum {

    /**
     * 服务监控
     */
    public static String JX3BOX_SERVER_URL = "https://spider.jx3box.com/jx3servers";
    /**
     * 日常监控
     */
    public static String JX3BOX_DAILY_URL = "https://team.api.jx3box.com/xoyo/daily/task?date=1647150300";
    /**
     * 官网新闻
     */
    public static String JX3BOX_WEBSITE_URL = "https://spider.jx3box.com/jx3news?client=std";
    /**
     * 官网地址
     */
    public static String JX3_WEBSITE_URL = "https://jx3.xoyo.com";
    /**
     * 金价
     */
    public static String JX3BOX_PRICE_GOLD = "https://spider.jx3box.com/jx3price";

    /**
     * 物品id
     */
    public static String JX3BOX_ITEMS_SEARCH_URL = "https://helper.jx3box.com/api/item/search?keyword=%E4%BA%8C%E5%8D%81%E5%9B%9B%E6%A1%A5%E6%98%8E%E6%9C%88%E5%A4%9C&page=1&limit=15";

    /**
     * 物价，根据物价id
     */
    public static String JX3BOX_PRICE_ITEMS_URL = "https://helper.jx3box.com/api/item/5_24427/price/logs?server=%E6%A2%A6%E6%B1%9F%E5%8D%97";

    /**
     * 宏，排行，全职业
     */
    public static String JX3BOX_MACRO_URL = "https://next2.jx3box.com/api/macro/overview?size=50&client=std";

    /**
     * 宏，按职业排行
     * https://www.jx3box.com/macro/#/rank?subtype=%E6%98%93%E7%AD%8B%E7%BB%8F
     */
    public static String JX3BOX_MACRO_TOP_URL = "https://next2.jx3box.com/api/macro/tops?kungfu=10002&size=50&client=std";

    /**
     * 副本攻略文章
     * 获取id，https://www.jx3box.com/fb/${id}
     */
    public static String JX3BOX_FB_URL = "https://cms.jx3box.com/api/cms/posts?type=fb&per=10&page=1&order=update&client=std";

    /**
     * 职业攻略-全职业
     * https://www.jx3box.com/bps/#/?subtype=%E8%8E%AB%E9%97%AE
     */
    public static String JX3BOX_CMS_URL = "https://cms.jx3box.com/api/cms/posts?type=bps&per=10&page=1&order=update&client=std";

    /**
     * 职业攻略-全职业
     * 成就百科
     */
    public static String JX3BOX_CJ_URL = "https://www.jx3box.com/cj/#/search/%E5%BE%80%E6%98%94%E7%81%AF%E8%B0%9C";

    /**
     * 教程工具
     */
    public static String JX3BOX_TOOLS_URL = "https://cms.jx3box.com/api/cms/posts?type=tool&per=10&page=1&order=update&client=std";

    /**
     * 技能查询
     */
    public static String JX3BOX_SKILLS_URL = "https://node.jx3box.com/skill/name/%E5%B0%8F%E4%B8%83?strict=0&per=10&page=1&client=std";

    /**
     * NPC查询
     */
    public static String JX3BOX_NPC_URL = "https://node.jx3box.com/npc/name/%E5%B0%8F%E4%B8%83?strict=0&per=10&page=1&client=std";

    /**
     * 捏脸
     */
    public static String JX3BOX_SHARE_URL = "https://cms.jx3box.com/api/cms/posts?type=share&per=40&sticky=1&page=1&mark=advanced";
    /**
     * 宠物
     * https://www.jx3box.com/pet/337
     */
    public static String JX3BOX_PET_URL = "https://node.jx3box.com/pets?per=16&page=1&Class=&Name=雀斑&Source=&client=std";

    /**
     * 奇遇
     * per=16&page=1&name=度人心
     */
    public static String ADVENTURE_URL = "https://node.jx3box.com/serendipities";
    public static String ADVENTURE_PATH = "https://www.jx3box.com/adventure";

    /**
     * 触发奇遇查询
     */
    public static String ADVENTURE_LOG = "https://pull.j3cx.com/api/serendipity?server=%E6%A2%A6%E6%B1%9F%E5%8D%97&role=%E4%B9%8C%E4%BA%91%E8%B8%8F%E9%9B%AA&serendipity=&start=0&pageIndex=1&pageSize=50";

}
