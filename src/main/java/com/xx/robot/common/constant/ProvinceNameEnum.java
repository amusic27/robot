package com.xx.robot.common.constant;

public enum ProvinceNameEnum {

    /**
     * 中国省份
     */
    HU_BEI("湖北省"),
    GUANG_DONG("广东省"),
    ZHE_JIANG("浙江省"),
    BEI_JING("北京市"),
    SHANG_HAI("上海市"),
    HU_NAN("湖南省"),
    AN_HUI("安徽省"),
    CHONG_QIONG("重庆市"),
    SI_CHUAN("四川省"),
    SHAN_DONG("山东省"),
    GUANG_XI("广西壮族自治区"),
    FU_JIAN("福建省"),
    JIANG_SU("江苏省"),
    HE_NAN("河南省"),
    HAI_NAN("海南省"),
    TIAN_JIN("天津市"),
    JIANG_XI("江西省"),
    SHAN_XI3("陕西省"),
    GUI_ZHOU("贵州省"),
    LIAO_NING("辽宁省"),
    XIANG_GANG("香港"),
    HEI_LONG_JIANG("黑龙江省"),
    AO_MEN("澳门"),
    XIN_JIANG("新疆维吾尔自治区"),
    GAN_SU("甘肃省"),
    YUN_NAN("云南省"),
    TAI_WAN("台湾"),
    SHAN_XI("山西省"),
    JI_LIN("吉林省"),
    HE_BEI("河北省"),
    NI_XIA("宁夏回族自治区"),
    NEI_MENG_GU("内蒙古自治区"),
    QING_HAI("青海省"),
    XI_ZANG("西藏自治区");

    private String name;

    ProvinceNameEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
