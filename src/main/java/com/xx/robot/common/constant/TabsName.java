package com.xx.robot.common.constant;

import java.util.HashMap;
import java.util.Map;

public class TabsName {

    public static final String bangding = "没绑定区服呢！快让群主绑定吧，口令：绑定 破阵子";
    public static String message = null;
    /**
     * 服务器
     */
    public static Map<String, Object> server = new HashMap<>();
    /**
     * 宏心法
     */
    public static Map<String, String> hongMap = new HashMap<>();
    /**
     * 沙盘编号
     */
    public static Map<String, String> shapan = new HashMap<>();
    /**
     * 开团坐标
     */
    public static Map<String, String> kaituan_index = new HashMap<>();
    /**
     * 心法分类
     */
    public static Map<String, String> hongMap_type = new HashMap<>();
    /**
     * 服务器ip
     */
    public static Map<String, String> server_ip = new HashMap<>();

    /**
     * 服务器ip
     */
    public static Map<String, String> BOX_SUB_TYPE = new HashMap<>();


    static {
        //区服字典
        server.put("斗转星移", "斗转星移");
        server.put("姨妈", "斗转星移");
        server.put("蝶恋花", "蝶恋花");
        server.put("龙争虎斗", "龙争虎斗");
        server.put("长安城", "长安城");
        server.put("幽月轮", "幽月轮");
        server.put("剑胆琴心", "剑胆琴心");
        server.put("煎蛋", "剑胆琴心");
        server.put("乾坤一掷", "乾坤一掷");
        server.put("华乾", "乾坤一掷");
        server.put("唯我独尊", "唯我独尊");
        server.put("唯满侠", "唯我独尊");
        server.put("梦江南", "梦江南");
        server.put("双梦", "梦江南");
        server.put("绝代天骄", "绝代天骄");
        server.put("绝代", "绝代天骄");
        server.put("破阵子", "破阵子");
        server.put("念破", "破阵子");
        server.put("天鹅坪", "天鹅坪");
        server.put("纵月", "天鹅坪");
        server.put("飞龙在天", "飞龙在天");
        server.put("大唐万象", "大唐万象");
        server.put("青梅煮酒", "青梅煮酒");
        server.put("共結來緣", "共結來緣");
        server.put("傲血戰意", "傲血戰意");
        server.put("巔峰再起", "巔峰再起");
        server.put("江海雲夢", "江海雲夢");
        //怀旧服
        server.put("缘起稻香", "缘起稻香");
        server.put("梦回长安", "梦回长安");
        server.put("烟雨扬州", "烟雨扬州");
        server.put("神都洛阳", "神都洛阳");
        server.put("天宝盛世", "天宝盛世");
        //宏查询
        hongMap.put("冰心诀", "冰心诀");
        hongMap.put("冰心", "冰心诀");

        hongMap.put("云裳心经", "云裳心经");
        hongMap.put("云裳", "云裳心经");
        hongMap.put("奶秀", "云裳心经");
        hongMap.put("秀奶", "云裳心经");

        hongMap.put("花间游", "花间游");
        hongMap.put("花间", "花间游");

        hongMap.put("离经易道", "离经易道");
        hongMap.put("离经", "离经易道");
        hongMap.put("奶花", "离经易道");
        hongMap.put("花奶", "离经易道");

        hongMap.put("毒经", "毒经");

        hongMap.put("补天诀", "补天诀");
        hongMap.put("奶毒", "补天诀");
        hongMap.put("毒奶", "补天诀");

        hongMap.put("莫问", "莫问");
        hongMap.put("相知", "相知");

        hongMap.put("奶歌", "相知");
        hongMap.put("歌奶", "相知");

        hongMap.put("无方", "无方");

        hongMap.put("灵素", "灵素");
        hongMap.put("药奶", "灵素");

        hongMap.put("傲血战意", "傲血战意");
        hongMap.put("傲血", "傲血战意");

        hongMap.put("铁牢律", "铁牢律");
        hongMap.put("策T", "铁牢律");
        hongMap.put("策t", "铁牢律");

        hongMap.put("易筋经", "易筋经");

        hongMap.put("洗髓经", "洗髓经");

        hongMap.put("焚影圣诀", "焚影圣诀");
        hongMap.put("焚影", "焚影圣诀");

        hongMap.put("明尊琉璃体", "明尊琉璃体");
        hongMap.put("明尊", "明尊琉璃体");
        hongMap.put("喵t", "明尊琉璃体");
        hongMap.put("喵T", "明尊琉璃体");

        hongMap.put("分山劲", "分山劲");
        hongMap.put("分山", "分山劲");

        hongMap.put("铁骨衣", "铁骨衣");
        hongMap.put("铁骨", "铁骨衣");

        hongMap.put("紫霞功", "紫霞功");
        hongMap.put("紫霞", "紫霞功");
        hongMap.put("气纯", "紫霞功");

        hongMap.put("太虚剑意", "太虚剑意");
        hongMap.put("剑纯", "太虚剑意");

        hongMap.put("天罗诡道", "天罗诡道");
        hongMap.put("田螺", "天罗诡道");

        hongMap.put("惊羽诀", "惊羽诀");
        hongMap.put("惊羽", "惊羽诀");
        hongMap.put("鲸鱼", "惊羽诀");

        hongMap.put("问水", "问水诀");
        hongMap.put("问水诀", "问水诀");

        hongMap.put("山居剑意", "山居剑意");
        hongMap.put("山居", "山居剑意");
        hongMap.put("藏剑", "山居剑意");
        hongMap.put("黄鸡", "山居剑意");
        hongMap.put("黄焖鸡", "山居剑意");

        hongMap.put("笑尘诀", "笑尘诀");
        hongMap.put("笑尘", "笑尘诀");
        hongMap.put("丐帮", "笑尘诀");

        hongMap.put("北傲诀", "北傲诀");
        hongMap.put("北傲", "北傲诀");
        hongMap.put("霸刀", "北傲诀");

        hongMap.put("凌海诀", "凌海诀");
        hongMap.put("凌海", "凌海诀");
        hongMap.put("蓬莱", "凌海诀");

        hongMap.put("隐龙诀", "隐龙诀");
        hongMap.put("隐龙", "隐龙诀");
        hongMap.put("凌雪", "隐龙诀");

        hongMap.put("太玄经", "太玄经");
        hongMap.put("太玄", "太玄经");
        hongMap.put("衍天", "太玄经");

        hongMap.put("通用", "通用");
        //沙盘
        shapan.put("斗转星移", "1");
        shapan.put("姨妈", "1");
        shapan.put("蝶恋花", "4");
        shapan.put("龙争虎斗", "5");
        shapan.put("长安城", "6");
        shapan.put("幽月轮", "7");
        shapan.put("剑胆琴心", "8");
        shapan.put("煎蛋", "8");
        shapan.put("乾坤一掷", "9");
        shapan.put("华乾", "9");
        shapan.put("唯我独尊", "10");
        shapan.put("唯满侠", "10");
        shapan.put("梦江南", "11");
        shapan.put("双梦", "11");
        shapan.put("绝代天骄", "12");
        shapan.put("绝代", "12");
        shapan.put("破阵子", "2");
        shapan.put("念破", "2");
        shapan.put("天鹅坪", "13");
        shapan.put("纵月", "13");
        shapan.put("飞龙在天", "14");
        shapan.put("大唐万象", "18");
        shapan.put("青梅煮酒", "15");
        //服务器ip
        server_ip.put("斗转星移", "125.88.195.133:3724");
        server_ip.put("蝶恋花", "125.88.195.112:3724");
        server_ip.put("龙争虎斗", "125.88.195.69:3724");
        server_ip.put("长安城", "125.88.195.52:3724");
        server_ip.put("幽月轮", "125.88.195.117:3724");
        server_ip.put("剑胆琴心", "125.88.195.42:3724");
        server_ip.put("乾坤一掷", "125.88.195.154:3724");
        server_ip.put("唯我独尊", "125.88.195.89:3724");
        server_ip.put("梦江南", "125.88.195.59:3724");
        server_ip.put("绝代天骄", "125.88.195.178:3724");
        server_ip.put("破阵子", "103.228.229.128:3724");
        server_ip.put("天鹅坪", "103.228.229.129:3724");
        server_ip.put("飞龙在天", "103.228.229.130:3724");
        server_ip.put("大唐万象", "125.88.195.38:3724");
        server_ip.put("青梅煮酒", "103.228.229.127:3724");
        server_ip.put("测试服务器", "49.232.54.156:8980");
        //心法//1内功，2外功，3t，4奶
        hongMap_type.put("冰心诀", "1");
        hongMap_type.put("冰心", "1");
        hongMap_type.put("云裳心经", "4");
        hongMap_type.put("云裳", "4");
        hongMap_type.put("奶秀", "4");
        hongMap_type.put("秀奶", "4");
        hongMap_type.put("花间游", "1");
        hongMap_type.put("花间", "1");
        hongMap_type.put("离经易道", "4");
        hongMap_type.put("离经", "4");
        hongMap_type.put("奶花", "4");
        hongMap_type.put("花奶", "4");
        hongMap_type.put("毒经", "1");
        hongMap_type.put("补天诀", "4");
        hongMap_type.put("奶毒", "4");
        hongMap_type.put("毒奶", "4");
        hongMap_type.put("莫问", "1");
        hongMap_type.put("相知", "4");
        hongMap_type.put("奶歌", "4");
        hongMap_type.put("歌奶", "4");
        hongMap_type.put("无方", "1");
        hongMap_type.put("灵素", "4");
        hongMap_type.put("药奶", "4");
        hongMap_type.put("傲血战意", "2");
        hongMap_type.put("傲血", "2");
        hongMap_type.put("铁牢律", "3");
        hongMap_type.put("策T", "3");
        hongMap_type.put("易筋经", "1");
        hongMap_type.put("洗髓经", "3");
        hongMap_type.put("焚影圣诀", "1");
        hongMap_type.put("焚影", "1");
        hongMap_type.put("明尊琉璃体", "3");
        hongMap_type.put("明尊", "3");
        hongMap_type.put("喵t", "3");
        hongMap_type.put("喵T", "3");
        hongMap_type.put("分山劲", "2");
        hongMap_type.put("分山", "2");
        hongMap_type.put("铁骨衣", "3");
        hongMap_type.put("铁骨", "3");
        hongMap_type.put("紫霞功", "1");
        hongMap_type.put("紫霞", "1");
        hongMap_type.put("气纯", "1");
        hongMap_type.put("太虚剑意", "2");
        hongMap_type.put("剑纯", "2");
        hongMap_type.put("天罗诡道", "1");
        hongMap_type.put("田螺", "1");
        hongMap_type.put("惊羽诀", "2");
        hongMap_type.put("惊羽", "2");
        hongMap_type.put("鲸鱼", "2");
        hongMap_type.put("问水", "2");
        hongMap_type.put("问水诀", "2");
        hongMap_type.put("山居剑意", "2");
        hongMap_type.put("黄鸡", "2");
        hongMap_type.put("黄焖鸡", "2");
        hongMap_type.put("藏剑", "2");
        hongMap_type.put("山居", "2");
        hongMap_type.put("笑尘诀", "2");
        hongMap_type.put("笑尘", "2");
        hongMap_type.put("丐帮", "2");
        hongMap_type.put("北傲诀", "2");
        hongMap_type.put("北傲", "2");
        hongMap_type.put("霸刀", "2");
        hongMap_type.put("凌海诀", "2");
        hongMap_type.put("凌海", "2");
        hongMap_type.put("蓬莱", "2");
        hongMap_type.put("隐龙诀", "2");
        hongMap_type.put("隐龙", "2");
        hongMap_type.put("凌雪", "2");
        hongMap_type.put("太玄经", "1");
        hongMap_type.put("太玄", "1");
        hongMap_type.put("衍天", "1");
        hongMap_type.put("老板", "5");

        BOX_SUB_TYPE.put("冰心诀", "10081");
        BOX_SUB_TYPE.put("云裳心经", "10080");
        BOX_SUB_TYPE.put("花间游", "10021");
        BOX_SUB_TYPE.put("离经易道", "10028");
        BOX_SUB_TYPE.put("毒经", "10175");
        BOX_SUB_TYPE.put("补天诀", "10176");
        BOX_SUB_TYPE.put("莫问", "10447");
        BOX_SUB_TYPE.put("相知", "10448");
        BOX_SUB_TYPE.put("无方", "10627");
        BOX_SUB_TYPE.put("灵素", "10626");
        BOX_SUB_TYPE.put("傲血战意", "10026");
        BOX_SUB_TYPE.put("铁牢律", "10062");
        BOX_SUB_TYPE.put("易筋经", "10003");
        BOX_SUB_TYPE.put("洗髓经", "10002");
        BOX_SUB_TYPE.put("焚影圣诀", "10242");
        BOX_SUB_TYPE.put("明尊琉璃体", "10243");
        BOX_SUB_TYPE.put("分山劲", "10390");
        BOX_SUB_TYPE.put("铁骨衣", "10389");
        BOX_SUB_TYPE.put("紫霞功", "10014");
        BOX_SUB_TYPE.put("太虚剑意", "10015");
        BOX_SUB_TYPE.put("天罗诡道", "10225");
        BOX_SUB_TYPE.put("惊羽诀", "10224");
        BOX_SUB_TYPE.put("问水诀", "10144");
        BOX_SUB_TYPE.put("山居剑意", "10145");
        BOX_SUB_TYPE.put("笑尘诀", "10268");
        BOX_SUB_TYPE.put("北傲诀", "10464");
        BOX_SUB_TYPE.put("凌海诀", "10533");
        BOX_SUB_TYPE.put("隐龙诀", "10585");
        BOX_SUB_TYPE.put("太玄经", "10615");
    }
}
