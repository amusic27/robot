package com.xx.robot.common.util;

import com.xx.robot.service.GroupMessage;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class SpringContextUtil implements ApplicationContextAware {
    public static MongoDbUtil mongoDbUtil = null;
    public static CqMessageUtil cqMessageUtil = null;
    public static GroupMessage groupMessage = null;

    private static ApplicationContext applicationContext = null;

    public SpringContextUtil() {

    }

    public void init() {
        if (mongoDbUtil == null) {
            mongoDbUtil = SpringContextUtil.getBean(MongoDbUtil.class);
        }
        if (cqMessageUtil == null) {
            cqMessageUtil = SpringContextUtil.getBean(CqMessageUtil.class);
        }
        if (groupMessage == null) {
            groupMessage = SpringContextUtil.getBean(GroupMessage.class);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static <T> T getBean(Class<T> beanName) {
        return applicationContext.getBean(beanName);
    }

    public static String getMessage(String key) {
        return applicationContext.getMessage(key, null, Locale.getDefault());
    }

}