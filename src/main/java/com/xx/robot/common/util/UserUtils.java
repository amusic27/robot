package com.xx.robot.common.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONObject;
import com.xx.robot.common.bean.CpHttpConfigBean;

/**
 * 杂七杂八的一些通用方法
 */
public class UserUtils {

    public static final String ADMIN = "owner";
    public static final String MANAGER = "admin";
    public static final String MESSAGE_AT_QQ = CpHttpConfigBean.MESSAGE_AT_QQ;
    public static final String NO_OPEN = CpHttpConfigBean.NO_OPEN;

    /**
     * 判断是不是管理者和机器人
     */
    public static boolean isAdminAndManagerAndBot(JSONObject user) {
        return isAdmin(user) || isManager(user) || isBot(user);
    }

    /**
     * 判断是不是管理者
     */
    public static boolean isAdminAndManager(JSONObject user) {
        return isAdmin(user) || isManager(user);
    }

    /**
     * 判断是不是群主
     */
    public static boolean isAdmin(JSONObject user) {
        String role = user.getStr("role");
        return CharSequenceUtil.equals(ADMIN, role);
    }

    /**
     * 判断是不是管理员
     */
    public static boolean isManager(JSONObject user) {
        String role = user.getStr("role");
        return CharSequenceUtil.equals(MANAGER, role);
    }

    /**
     * 判断是不是机器人
     */
    public static boolean isBot(JSONObject user) {
        String userId = user.getStr("user_id");
        return CharSequenceUtil.equals(MESSAGE_AT_QQ, userId) || CharSequenceUtil.equals(NO_OPEN, userId);
    }
}
