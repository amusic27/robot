package com.xx.robot.common.util.gocphttp;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xx.robot.common.util.message.PublicMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import com.xx.robot.common.bean.CpHttpConfigBean;
import com.xx.robot.common.constant.CollectionNameEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * cq发送消息util
 *
 * @author Lenovo
 */
@Slf4j
@Component
public class CqMessageUtil {

    /**
     * 发送消息
     *
     * @param qqCode  对方qq
     * @param message 消息内容
     *                //     * @param bon 内容中是否携带qq表情
     */
    public static void privateSend(String qqCode, String message) {
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("user_id", qqCode);
        paramMap.put("message", message);
        paramMap.put("auto_escape", "false");
        HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS, paramMap);
    }

    /**
     * 发送消息
     *
     * @param groupId 群号
     * @param message 消息
     */
    public static void groupSend(String groupId, String message) {
        groupSend(groupId, message, "true");
    }

    /**
     * 发送消息
     *
     * @param groupId 群号
     * @param message 消息
     * @param bon     是否解析表情包
     */
    public static void groupSend(String groupId, String message, String bon) {
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("group_id", groupId);
        paramMap.put("message", message);
        paramMap.put("auto_escape", bon);
        HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/send_group_msg", paramMap);

        // 增加到复制
        PublicMessageUtil.addCopy(groupId, message);
    }

    /**
     * 发送到绑定的所有群
     *
     * @param message 群发的消息
     */
    public static void groupSendAll(String message) {
        List<JSONObject> serverLists = MongoDbUtil.selectAll(JSONObject.class, CollectionNameEnum.JX3_SERVER.getCode());
        log.info("绑定服务器：{}", serverLists);
        if (CollUtil.isNotEmpty(serverLists)) {
            serverLists.forEach(o -> groupSend(o.getStr("_id"), message));
        }
        log.info("群发消息：{}", message);
    }

    /**
     * 获取所有群列表
     *
     * @return 群列表
     */
    public static List<Object> allGroupId() {
        String result = HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/get_group_list");
        if (JSONUtil.isTypeJSON(result)) {
            JSONObject groupInfoList = JSONUtil.parseObj(result);
            log.info(groupInfoList.toString());
            return groupInfoList.getJSONArray("group_id");
        }
        return new ArrayList<>();
    }

    /**
     * 自动审核群申请
     * <p>
     * flag	string	-	加群请求的 flag（需从上报的数据中获得）
     * sub_type 或 type	string	-	add 或 invite, 请求类型（需要和上报消息中的 sub_type 字段相符）
     * approve	boolean	true	是否同意请求／邀请
     * reason	string	空	拒绝理由（仅在拒绝时有效）
     *
     * @param map 参数
     */
    public static void groupRequest(Map<String, Object> map) {
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("flag", map.get("flag"));
        paramMap.put("sub_type", map.get("sub_type"));
        paramMap.put("approve", map.get("approve"));
        paramMap.put("reason", map.get("reason"));
        HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/set_group_add_request", paramMap);
    }

    /**
     * 自动审核好友申请
     * <p>
     * flag	string	-	加好友请求的 flag（需从上报的数据中获得）
     * approve	boolean	true	是否同意请求
     * remark	string	空	添加后的好友备注（仅在同意时有效）
     *
     * @param map 参数
     */
    public static void friendRequest(Map<String, Object> map) {
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("flag", map.get("flag"));
        paramMap.put("approve", map.get("approve"));
        paramMap.put("remark", map.get("remark"));
        HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/set_friend_add_request", paramMap);
    }


    /**
     * 获取群成员列表
     *
     * @param groupId 群id
     */
    public static JSONArray groupMemberList(String groupId) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("group_id", groupId);
        String result = HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/get_group_member_list", paramMap);
        return JSONUtil.parseObj(result).getJSONArray("data");
    }

    /**
     * 查询群中固定的好友信息
     *
     * @param groupId 群id
     */
    public static JSONObject groupFriendInfo(String groupId, String userId) {
        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("group_id", groupId);
        paramMap.put("user_id", userId);
        paramMap.put("no_cache", "false");
        String result = HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/get_group_member_list", paramMap);
        return JSONUtil.parseObj(result).getJSONObject("data");
    }


    /**
     * 群组单人禁言
     *
     * @param groupId  群号
     * @param userId   要禁言的 QQ 号
     * @param duration 禁言时长，单位秒，0 表示取消禁言
     */
    public static void bannedToPost(String groupId, String userId, String duration) {

        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("group_id", groupId);
        paramMap.put("user_id", userId);
        paramMap.put("duration", duration);
        HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/set_group_ban", paramMap);
    }

    /**
     * 获取群荣誉信息
     *
     * @param groupId 群号
     */
    public static void groupHonorInfo(String groupId) {

        Map<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("group_id", groupId);
        paramMap.put("type", "talkative");
        log.info(HttpUtil.get(CpHttpConfigBean.MESSAGE_HTTPS + "/get_group_honor_info", paramMap));
    }
}
