package com.xx.robot.common.util.message;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.common.vo.message.GroupMessage;
import com.xx.robot.common.vo.message.MessageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 消息公共处理类
 *
 * @author LenovoPublicMessage
 */
@Slf4j
public class PublicMessageUtil {
    /**
     * 群聊记录，只记录三条，用于检测是否复制
     */
    public static Map<String, GroupMessage> map = new HashMap<>();
    /**
     * 群聊定时检测，是否有其他机器人，如果有停止服务
     */
    public static Map<String, Boolean> sendMessage = new HashMap<>();

    /**
     * 存储信息
     *
     * @param groupId 群id
     * @param message 消息
     */
    public static void addMessage(String groupId, String message, Map<String, Object> user) {
        String userId = user.get("user_id").toString();

        if (map.containsKey(groupId)) {
            //存在
            GroupMessage groupMessage = map.get(groupId);
            //用户id
            MessageInfo messageInfo = new MessageInfo(userId, message, DateUtil.now(), user);
            groupMessage.setInfos(messageInfo);
        } else {
            //不存在
            //用户id
            MessageInfo messageInfo = new MessageInfo(userId, message, DateUtil.now(), user);
            GroupMessage groupMessage = new GroupMessage(groupId, messageInfo);
            map.put(groupId, groupMessage);
        }
    }

    /**
     * 判断是否复制消息
     *
     * @param message 聊天消息
     * @return true 可以复制，false 不可以复制
     */
    public static boolean isCopyMessage(String groupId, String message) {
        boolean isCopy = false;
        if (map.containsKey(groupId)) {
            //存在
            GroupMessage groupMessage = map.get(groupId);
            List<MessageInfo> infos = groupMessage.getInfos();
            if (CollUtil.isEmpty(infos)) {
                return false;
            }
            //判断当前聊天是否已经复制过了
            if (message.equals(groupMessage.getCopy())) {
                return false;
            }

            // 是否存在相同的聊天记录
            isCopy = infos.stream().allMatch(o -> StrUtil.equals(o.getMessage(), message));
            //存发送的消息
            if (isCopy) {
                groupMessage.setCopy(message);
                log.info("复制消息:{}", message);
            }
        }

        return isCopy;

    }

    public static void addCopy(String groupId, String message) {
        if (map.containsKey(groupId)) {
            GroupMessage groupMessage = map.get(groupId);
            if (groupMessage != null) {
                groupMessage.setCopy(message);
            }
        }
    }


    /**
     * 返回绑定区服
     *
     * @param groupId
     * @return
     */
    public static String bindServer(String groupId) {
        JSONObject serverJson = MongoDbUtil.selectById(groupId, JSONObject.class, CollectionNameEnum.JX3_SERVER.getCode());
        if (CollectionUtils.isEmpty(serverJson)) {
            return CharSequenceUtil.EMPTY;
        }
        return serverJson.getStr("server");
    }

    /**
     * 判断是否绑定区服
     *
     * @param groupId
     */
    public static void isBindServer(String groupId) {
        JSONObject serverJson = MongoDbUtil.selectById(groupId, JSONObject.class, CollectionNameEnum.JX3_SERVER.getCode());
        if (CollectionUtils.isEmpty(serverJson)) {
            CqMessageUtil.groupSend(groupId, "没绑定区服呢！快让群主绑定吧，口令：绑定 梦江南");
        }
    }

}
