package com.xx.robot.common.util.mongo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MongoPage<T> implements Serializable {

    private long total;
    private long num;
    private int size;
    private Object data;


    public MongoPage() {
    }

    public MongoPage(long num, int size) {
        this.num = num;
        this.size = size;
    }

    public long getSkip() {
        return this.num;
    }

}
