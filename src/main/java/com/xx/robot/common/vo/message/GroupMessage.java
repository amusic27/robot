package com.xx.robot.common.vo.message;

import cn.hutool.core.collection.CollUtil;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 * 群对象信息
 *
 * @author Lenovo
 * @Name GroupMessage
 */
public class GroupMessage {

    private String groupId;
    /**
     * 聊天记录
     */
    private List<MessageInfo> infos = new ArrayList<>();
    private String copy;

    public GroupMessage() {
    }

    public GroupMessage(String groupId, MessageInfo info) {
        this.groupId = groupId;
        //当infos 有数据且大于等于3时 删除第一个。
        if (CollUtil.isNotEmpty(infos) && infos.size() >= 2) {
            infos.remove(0);
        }
        if (CollectionUtils.isEmpty(infos)) {
            infos = new ArrayList<>();
            infos.add(info);
        } else {
            infos.add(info);
        }

    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }

    public List<MessageInfo> getInfos() {
        return infos;
    }

    public void setInfos(MessageInfo info) {
        //当infos 有数据且大于等于3时 删除第一个。
        if (!CollectionUtils.isEmpty(infos) && infos.size() >= 2) {
            infos.remove(0);
        }
        if (CollectionUtils.isEmpty(infos)) {
            infos = new ArrayList<>();
            infos.add(info);
        } else {
            infos.add(info);
        }
    }
}
