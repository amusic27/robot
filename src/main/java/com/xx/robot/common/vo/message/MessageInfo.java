package com.xx.robot.common.vo.message;

import lombok.Data;

import java.util.Map;

/**
 * TODO
 * 消息体对象
 *
 * @author Lenovo
 * @Name MessageInfo
 */
@Data
public class MessageInfo {
    private String code;
    private String message;
    private String dataTime;
    private Map<String, Object> user;

    public MessageInfo() {

    }

    public MessageInfo(String code, String message, String dataTime, Map<String, Object> user) {
        this.code = code;
        this.message = message;
        this.dataTime = dataTime;
        this.user = user;
    }

}
