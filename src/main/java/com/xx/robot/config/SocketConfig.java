package com.xx.robot.config;

import com.xx.robot.socket.client.GoCqHttpSocket;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

@Slf4j
@Configuration
public class SocketConfig {

    @Bean
    public WebSocketClient GoCqHttpClient() {
        GoCqHttpSocket goCqHttpSocket = null;
        try {
            goCqHttpSocket = new GoCqHttpSocket();
            goCqHttpSocket.setConnectionLostTimeout(0);
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
        }
        goCqHttpSocket.connect();
        //如果断线，则重连并重新发送验证信息
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!GoCqHttpSocket.WEB_STATUS) {
                    try {
                        GoCqHttpSocket goCqHttpSocket = new GoCqHttpSocket();
                        goCqHttpSocket.setConnectionLostTimeout(0);
                        goCqHttpSocket.connect();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 5000, 5000);//5秒执行一次 然后休息5秒
        return goCqHttpSocket;
    }
}
