package com.xx.robot.cronjob;

import com.xx.robot.service.Covid19DxyService;
import com.xx.robot.service.HotSearchService;
import com.xx.robot.service.Jx3BoxService;
import com.xx.robot.service.NewsService;
import com.xx.robot.common.util.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;

/**
 * @author liush
 */
@Slf4j
@Configuration
@EnableScheduling
public class SchedulerTask {
    @Autowired
    private SpringContextUtil springContextUtil;
    @Autowired
    private HotSearchService hotSearchService;
    @Autowired
    private Covid19DxyService covid19DxyService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private Jx3BoxService jx3BoxService;

    /**
     * 官网新闻,服务器监控
     * 5min 爬一次
     */
    @Scheduled(cron = "0 0/5 * * * ? ")
    public void jx3News() {
        jx3BoxService.statusMonitor();
        jx3BoxService.news();
    }


    /**
     * 微博热搜
     * 阿B热搜
     * 1h 爬一次
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void hotWeiboCrawler() {
        hotSearchService.hotWeiboCrawler();

        hotSearchService.hotBilibiliCrawler();
        hotSearchService.rankBilibiliCrawler();
    }

    /**
     * 60s读世界，每天早7点更新
     */
    @Scheduled(cron = "0 0 8 * * ?")
    public void world60sNews() {
        newsService.world60s();
    }

    /**
     * 每天9点，15点开始摸鱼
     */
    @Scheduled(cron = "0 0 9,15 * * ?")
    public void fishRemind() {
        newsService.fishRemindImg();
    }

    /**
     * 测试
     * 1m 爬一次
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void test() {
        log.info("测试");
    }

    @PostConstruct
    public void action() {
        System.out.println("初始化Spring boot,静态注入");
        springContextUtil.init();
    }

}