package com.xx.robot.service;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONObject;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * 定制服务
 *
 * @author liush
 */
@Service
@Slf4j
public class CustomService {

    /**
     * 查询极简按键密码
     *
     * @param groupId 群名
     */
    public void jiJianPassword(String groupId) {
        // 缓存的最新的一条
        Query query = new Query().with(Sort.by(Sort.Direction.DESC, "_id"));
        JSONObject jiJianObj = MongoDbUtil.selectOneByQuery(query, JSONObject.class, CollectionNameEnum.OTHER_JIJIAN_ANJIAN.getCode());

        CqMessageUtil.groupSend(groupId, "最新密码是：" + jiJianObj.getStr("password"));
    }

    /**
     * 更新极简按键密码
     *
     * @param groupId 群名
     * @param message 消息
     */
    public void updatePassword(String groupId, String message) {
        log.info("消息：{}", message);
        message = CharSequenceUtil.replace(message, "更新极简按键密码", CharSequenceUtil.EMPTY).trim();
        log.info("密码是：{}", message);
        JSONObject data = new JSONObject();
        data.set("password", message);
        data.set("update_time", DateUtil.now());
        MongoDbUtil.save(data, CollectionNameEnum.OTHER_JIJIAN_ANJIAN.getCode());
        CqMessageUtil.groupSend(groupId, "密码更新成功");
    }
}
