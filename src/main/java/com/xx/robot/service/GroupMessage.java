package com.xx.robot.service;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONObject;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.common.constant.TabsName;
import com.xx.robot.common.util.UserUtils;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.message.PublicMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import com.xx.robot.thrid.JokeUtil;
import com.xx.robot.thrid.PigImageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class GroupMessage {

    @Autowired
    private Jx3ApiService jx3ApiService;
    @Autowired
    private NiggerRankService niggerRankService;
    @Autowired
    private Jx3BoxService jx3BoxService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private HotSearchService hotSearchService;

    @Autowired
    private CustomService customService;
    @Autowired
    private PigImageUtil pigImageUtil;

    /**
     * 群消息自动回复命令分发
     *
     * @param messageJson 消息体
     * @param sender      用户信息
     */
    public void distribute(JSONObject messageJson, JSONObject sender) {
        //群号
        String groupId = messageJson.getStr("group_id");
        //指令
        String message = messageJson.getStr("message").trim();

        //检测当前群中是否包含特定qq号，如果包含，便不服务
        if (PublicMessageUtil.sendMessage.containsKey(groupId)) {
            return;
        }
        switch (message) {
            //固定菜单分配
            case "测试2" -> CqMessageUtil.groupHonorInfo(groupId);

            case "菜单", "功能" -> menus(groupId);
            case "公告", "更新公告" -> jx3BoxService.webNews(groupId);

            case "开服" -> {
                String server = PublicMessageUtil.bindServer(groupId);
                jx3BoxService.serverStatus(groupId, server);
            }
            case "日常" -> jx3ApiService.daily(groupId);

            case "开启新闻头条" -> newsService.newsSwitch(groupId, 0, "news_world60s", sender);
            case "关闭新闻头条" -> newsService.newsSwitch(groupId, 1, "news_world60s", sender);
            case "开启摸鱼提醒" -> newsService.newsSwitch(groupId, 0, "fish_remind", sender);
            case "关闭摸鱼提醒" -> newsService.newsSwitch(groupId, 1, "fish_remind", sender);

            case "微博热搜" -> {
                String hotWeibo = hotSearchService.hotWeibo();
                if (CharSequenceUtil.isNotBlank(hotWeibo)) {
                    CqMessageUtil.groupSend(groupId, hotWeibo);
                }
            }
            case "B站热门", "B站综合热门", "b站热门", "b站综合热门" -> {
                String hotBilibili = hotSearchService.hotBilibili();
                if (CharSequenceUtil.isNotBlank(hotBilibili)) {
                    CqMessageUtil.groupSend(groupId, hotBilibili);
                }
            }
            case "B站排行", "B站排行榜", "b站排行", "b站排行榜" -> {
                String rankBilibili = hotSearchService.rankBilibili();
                if (CharSequenceUtil.isNotBlank(rankBilibili)) {
                    CqMessageUtil.groupSend(groupId, rankBilibili);
                }
            }

            case "小本本" -> niggerRankService.xbb(groupId);
            case "红黑榜" -> niggerRankService.blackAndRed(groupId);
            case "小红手", "欧皇榜" -> niggerRankService.list(groupId, true);
            case "小黑手", "黑鬼榜" -> niggerRankService.list(groupId, false);

            case "段子", "情话" -> CqMessageUtil.groupSend(groupId, JokeUtil.loveWords());
            case "骚话" -> CqMessageUtil.groupSend(groupId, JokeUtil.saoWords());

            case "极简按键密码" -> customService.jiJianPassword(groupId);

            case "舔狗日记", "剑纯日记", "日记" -> jx3ApiService.tiangou(groupId);
            case "老婆", "二次元", "妹子" -> {
                String pigjpg = pigImageUtil.pigImage();
                if (CharSequenceUtil.isNotBlank(pigjpg)) {
                    CqMessageUtil.groupSend(groupId, pigjpg, "false");
                }
            }

            default -> { //模糊匹配分配
                // 添加小本本
                if (message.contains("团名:") && message.contains("原因:")) {
                    niggerRankService.addXbb(groupId, message, sender);
                }
                // 查询小本本
                if (message.startsWith("小本本") && message.length() <= 15) {
                    niggerRankService.findXbb(groupId, message);
                }

                // 添加红黑榜
                boolean blackAndRedFlag = message.contains("欧皇:") || message.contains("黑鬼:");
                if (blackAndRedFlag && message.contains("原因:")) {
                    niggerRankService.addBlackAndRed(groupId, message, sender);
                }

                // 极简按键密码
                boolean updateJiJianFlag = message.startsWith("更新极简按键密码");
                if (updateJiJianFlag) {
                    customService.updatePassword(groupId, message);
                }

                //奇遇-攻略
                else if (message.length() >= 3 && message.length() <= 15 && message.startsWith("奇遇")) {
                    jx3BoxService.findQiYu(groupId, message);
                }
                //宏
                else if (message.length() >= 3 && message.length() <= 15 && message.startsWith("宏 ")) {
                    jx3BoxService.macroRank(groupId, message);
                }
                //物价
                else if (message.length() >= 4 && message.length() <= 15 && message.contains("物价")) {
                    jx3ApiService.priceInquiry(groupId, message);
                }
                //沙盘
                else if (message.length() >= 4 && message.length() <= 15 && message.contains("沙盘")) {
                    jx3ApiService.shapan(groupId, message);
                }
                //配装
                else if (message.length() >= 4 && message.length() <= 15 && message.contains("配装")) {
                    jx3ApiService.Peizhuang(groupId, message);
                }
                //金价
                else if (message.length() <= 15 && message.startsWith("金价")) {
                    jx3ApiService.jinjia(groupId, message);
                }

                //群[780591325],琴娘关键词,自动@具体人
                else if (message.length() <= 15 && message.contains("琴娘") && CharSequenceUtil.equals(groupId, "780591325")) {
                    CqMessageUtil.groupSend("780591325", "[CQ:at,qq=" + "493155129" + "] " + "你的琴娘来了,快来接驾", "false");
                }
                //群[780591325],琴娘关键词,自动@具体人
                else if (message.length() <= 15 && message.contains("甜心") && CharSequenceUtil.equals(groupId, "780591325")) {
                    CqMessageUtil.groupSend("780591325", "[CQ:at,qq=" + "1241649998" + "] " + "甜心哥哥呢~~快来给人家舔jiojio啦", "false");
                }
                //绑定区服
                else if (message.length() >= 3 && message.length() <= 15 && message.startsWith("绑定")) {
                    groupJx3Server(groupId, message, sender);
                }
                //自由语言
                else {
                    freedomMessage(groupId, message, sender);
                }
            }
        }

    }

    /**
     * 主页菜单
     *
     * @param groupId 群名
     */
    public void menus(String groupId) {
        String enums = """
                输入：【菜单】查询j3相关功能.
                其他娱乐功能请【/响应】开启，使用【/服务详情】查询具体命令:
                【日常】
                【小本本、红黑榜】
                【心法+宏】
                【配装+心法】
                【沙盘+区服】
                【物价+物品名称】
                【奇遇+名称】查询攻略
                【服务器名称】查询开服状态
                【舔狗日记,剑纯日记】
                【骚话,情话,段子】
                【区服+金价/金价+区服】
                【小红手/欧皇榜、小黑手/黑鬼榜】
                 ---------------------
                 开服通知(自动)
                 早8:00新闻60S(自动)
                 9:00,15:30摸鱼提醒(自动)
                 -------其他功能-------
                【段子】
                【B站热门、B站排行、微博热搜】
                【疫情+市级城市名称】疫情状态
                【老婆、妹子、二次元】二次元图片
                 -----设置自动推送-----
                【开启新闻头条/关闭新闻头条】
                【开启摸鱼提醒/关闭摸鱼提醒】
                 ---------------------
                 首次使用请绑定区服额
                 例:绑定 梦江南
                 ---------------------
                 自定义功能pypy""";
        CqMessageUtil.groupSend(groupId, enums);
    }


    /**
     * 自由发言
     *
     * @param groupId 群id
     * @param message 消息内容
     */
    public void freedomMessage(String groupId, String message, JSONObject user) {
        //开服内容查询
        if (message.length() <= 4 && TabsName.server.containsKey(message)) {
            jx3ApiService.server(groupId, TabsName.server.get(message).toString());
        }
        //自动检测复制
        if (PublicMessageUtil.isCopyMessage(groupId, message)) {
            CqMessageUtil.groupSend(groupId, message, "false");
        }
        //记录信息
        PublicMessageUtil.addMessage(groupId, message, user);
    }

    /**
     * 群绑定服务器
     *
     * @param groupId 群号
     * @param message 消息
     */
    public void groupJx3Server(String groupId, String message, JSONObject user) {
        if (!UserUtils.isAdmin(user)) {
            CqMessageUtil.groupSend(groupId, "你是群主吗？不是。是乌云的小跟班吗？也不是。那玩去吧吧");
        }
        String finalMessage = CharSequenceUtil.replace(message, "绑定", CharSequenceUtil.EMPTY).trim();

        String serverName = TabsName.server.entrySet().stream()
                .filter(e -> finalMessage.equals(e.getKey()))
                .map(Map.Entry::getValue)
                .findFirst().orElse(CharSequenceUtil.EMPTY).toString();

        if (CharSequenceUtil.isBlank(serverName)) {
            CqMessageUtil.groupSend(groupId, "没有找到要绑定的服务器呢");
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("_id", groupId);
        map.put("server", serverName);
        map.put("server_status", jx3BoxService.findServerStatus(serverName));
        MongoDbUtil.save(map, CollectionNameEnum.JX3_SERVER.getCode());

        CqMessageUtil.groupSend(groupId, "成功绑定" + serverName);
        log.info("群：{}，绑定区服:{}", groupId, serverName);
    }

}
