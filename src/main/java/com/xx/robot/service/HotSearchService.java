package com.xx.robot.service;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.thrid.HotSearchUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class HotSearchService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public String hotWeibo() {
        Query query = new Query().with(Sort.by(Sort.Direction.DESC, "hot")).limit(3);
        List<JSONObject> hashMaps = mongoTemplate.find(query, JSONObject.class, CollectionNameEnum.HOT_WEIBO.getCode());
        StringBuilder message = new StringBuilder();

        for (JSONObject o : hashMaps) {
            message.append("名称：").append(o.getStr("name")).append("\n热度：").append(o.getStr("hot")).append("\n链接：").append(o.getStr("url")).append("\n");
        }
        log.info(message.toString());

        return String.valueOf(message);
    }

    public String hotBilibili() {
        Query query = new Query().with(Sort.by(Sort.Direction.ASC, "rank")).limit(5);
        List<JSONObject> hashMaps = mongoTemplate.find(query, JSONObject.class, CollectionNameEnum.HOT_POPULAR_BILIBILI.getCode());
        StringBuilder message = new StringBuilder();

        for (JSONObject o : hashMaps) {
            message.append("名称：").append(o.getStr("name"))
                    .append("\nup主：").append(o.getStr("up"))
                    .append("\n链接：").append(o.getStr("url")).append("\n\n");
        }
        log.info(message.toString());

        return String.valueOf(message);
    }

    public String rankBilibili() {
        Query query = new Query().with(Sort.by(Sort.Direction.ASC, "rank")).limit(5);
        List<JSONObject> hashMaps = mongoTemplate.find(query, JSONObject.class, CollectionNameEnum.HOT_RANKING_BILIBILI.getCode());
        StringBuilder message = new StringBuilder();

        for (JSONObject o : hashMaps) {
            message.append("名称：").append(o.getStr("name"))
                    .append("\nup主：").append(o.getStr("up"))
                    .append("\n链接：").append(o.getStr("url")).append("\n");
        }
        log.info(message.toString());

        return String.valueOf(message);
    }

    public void hotWeiboCrawler() {
        String result = HotSearchUtil.hotWeibo();
        JSONObject resultJson = JSONUtil.parseObj(result);
        String code = resultJson.getStr("data");
        if (CharSequenceUtil.equals("200", code)) {
            JSONArray list = resultJson.getJSONArray("list");
            long time = System.currentTimeMillis();
            for (Object o : list) {
                JSONObject jsonObject = (JSONObject) o;
                jsonObject.set("time", time);
                mongoTemplate.insert(o, CollectionNameEnum.HOT_WEIBO.getCode());
            }
            //缓存成功，删除历史的数据
            mongoTemplate.remove(new Query(Criteria.where("time").ne(time)), CollectionNameEnum.HOT_WEIBO.getCode());
        }
    }

    public void hotBilibiliCrawler() {
        JSONArray jsonArray = HotSearchUtil.hotBilibili();
        long time = System.currentTimeMillis();
        for (int i = 0, jsonArraySize = jsonArray.size(); i < jsonArraySize; i++) {
            JSONObject result = new JSONObject();
            JSONObject o = (JSONObject) jsonArray.get(i);
            result.set("rank", i + 1);
            result.set("name", o.getStr("title"));
            result.set("up", o.getJSONObject("owner").getStr("name"));
            result.set("url", o.getStr("short_link"));
            result.set("time", time);
            mongoTemplate.insert(result, CollectionNameEnum.HOT_POPULAR_BILIBILI.getCode());
        }
        //缓存成功，删除历史的数据
        mongoTemplate.remove(new Query(Criteria.where("time").ne(time)), CollectionNameEnum.HOT_POPULAR_BILIBILI.getCode());
    }

    public void rankBilibiliCrawler() {
        JSONArray jsonArray = HotSearchUtil.rankBilibili();
        long time = System.currentTimeMillis();
        for (int i = 0, jsonArraySize = jsonArray.size(); i < jsonArraySize; i++) {
            JSONObject result = new JSONObject();
            JSONObject o = (JSONObject) jsonArray.get(i);
            result.set("rank", i + 1);
            result.set("name", o.getStr("title"));
            result.set("up", o.getJSONObject("owner").getStr("name"));
            result.set("url", o.getStr("short_link"));
            result.set("time", time);
            mongoTemplate.insert(result, CollectionNameEnum.HOT_RANKING_BILIBILI.getCode());
        }
        log.info("刷新 {} ", CollectionNameEnum.HOT_RANKING_BILIBILI.getCode());
        //缓存成功，删除历史的数据
        mongoTemplate.remove(new Query(Criteria.where("time").ne(time)), CollectionNameEnum.HOT_RANKING_BILIBILI.getCode());
        log.info("删除 {} 历史数据", CollectionNameEnum.HOT_RANKING_BILIBILI.getCode());
    }
}
