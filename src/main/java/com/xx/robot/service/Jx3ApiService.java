package com.xx.robot.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.ChineseDate;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.message.PublicMessageUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.common.constant.TabsName;
import com.xx.robot.thrid.Jx3ApiHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class Jx3ApiService {


    /**
     * 获取默认日常信息
     */
    public void daily(String groupId) {
        String serverName = PublicMessageUtil.bindServer(groupId);
        if (ObjectUtil.isNull(serverName)) {
            CqMessageUtil.groupSend(groupId, TabsName.bangding);
            return;
        }

        String result = Jx3ApiHttpUtil.daily(serverName);
        if (JSONUtil.isTypeJSON(result)) {
            JSONObject resultJson = JSONUtil.parseObj(result);
            if (resultJson.getInt("code") == 200) {
                JSONObject data = resultJson.getJSONObject("data");
                Date today = DateUtil.date();
                int year = DateUtil.year(today);
                ChineseDate date = new ChineseDate(DateUtil.parseDate(today.toString()));

                StringBuilder message = new StringBuilder();
                message.append("今天是：").append(DateUtil.today())
                        .append("   周").append(data.getStr("week")).append("    第")
                        .append(DateUtil.dayOfYear(today)).append("/").append(DateUtil.lengthOfYear(year)).append("天。\n农历：")
                        .append(date).append("(").append(date.getCyclicalYMD()).append(")");

                if (CharSequenceUtil.isNotBlank(date.getFestivals())) {
                    message.append("\n节日：").append(date.getFestivals());
                }

                message.append("\n\n大战：").append(data.get("war")).append("\n");
                message.append("战场：").append(data.get("battle")).append("\n");
                message.append("矿跑：").append(data.get("camp")).append("\n");
                message.append("公共日常：").append(data.get("relief")).append("\n");

                String draw = data.getStr("draw");
                if (CharSequenceUtil.isNotBlank(draw)) {
                    message.append("美人图：").append(draw).append("\n");
                }

                JSONArray teamList = data.getJSONArray("team");
                message.append("公共周常：").append(teamList.get(0)).append("\n");
                message.append("五人周常：").append(teamList.get(1)).append("\n");
                message.append("十人周常：").append(teamList.get(2));
                CqMessageUtil.groupSend(groupId, message.toString().replace(";", StrPool.COMMA));
            }
        }
    }

    /**
     * 服务器状态查询
     *
     * @param message 服务器
     */
    public void server(String groupId, String message) {

        String result = Jx3ApiHttpUtil.server(message);
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "服务器状态查询API出问题了，嘤嘤嘤");
            return;
        }
        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "服务器状态查询API出问题了，嘤嘤嘤");
            return;
        }
        StringBuilder str = new StringBuilder();
        JSONObject data = resultJson.getJSONObject("data");
        str.append("区服：").append(data.getStr("server")).append("\n");
        str.append("状态：").append(data.getInt("status") == 1 ? "已开服,侠士可充值188畅玩大美江湖" : "已关服,充值188,是兄弟就来砍我!.");
        CqMessageUtil.groupSend(groupId, str.toString());
    }


    /**
     * 服务器金价查询
     *
     * @param message 服务器
     */
    public void jinjia(String groupId, String message) {
        String server = "";
        message = message.replace(" ", "");


        if (CharSequenceUtil.equals(message, "金价")) {
            server = MongoDbUtil.selectById(groupId, JSONObject.class, CollectionNameEnum.JX3_SERVER.getCode()).getStr("server");
        } else {
            message = message.replace("金价", "");

            for (String key : TabsName.server.keySet()) {
                if (message.contains(key)) {
                    server = TabsName.server.get(key).toString();
                }
            }
            if (CharSequenceUtil.isEmpty(server)) {
                CqMessageUtil.groupSend(groupId, "这是哪个服？辣鸡西山居还没出的吧");
                return;
            }
        }

        String result = Jx3ApiHttpUtil.jinjia(server);
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "服务器金价查询API出问题了，嘤嘤嘤");
            return;
        }
        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "服务器金价查询API出问题了，嘤嘤嘤");
            return;
        }
        StringBuilder str = new StringBuilder();

        List<Object> data = resultJson.getJSONArray("data");
        Map<String, Object> maps = (JSONObject) data.get(0);
        str.append(maps.get("server")).append(" 最新金价汇报如下：\n");
        str.append("万宝楼：").append(maps.get("wanbaolou")).append("\n");
        str.append("贴吧：").append(maps.get("tieba")).append("\n");
        str.append("dd373：").append(maps.get("dd373")).append("\n");
        str.append("uu898：").append(maps.get("uu898")).append("\n");
        str.append("5173：").append(maps.get("5173")).append("\n");
        str.append("7881：").append(maps.get("7881")).append("\n");

        CqMessageUtil.groupSend(groupId, str.toString());
    }

    /**
     * 添狗日记
     */
    public void tiangou(String groupId) {
        String result = Jx3ApiHttpUtil.tiangou();
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "添狗日记API出问题了，嘤嘤嘤");
            return;
        }
        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "添狗日记API出问题了，嘤嘤嘤");
            return;
        }
        CqMessageUtil.groupSend(groupId, resultJson.getJSONObject("data").getStr("text"));
    }


    /**
     * 物价查询
     */
    public void priceInquiry(String groupId, String message) {
        message = message.replace("物价", "").trim();

        String result = Jx3ApiHttpUtil.PriceInquiry(message);
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "物价查询API出问题了，嘤嘤嘤");
            return;
        }
        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "物价查询API出问题了，嘤嘤嘤");
            return;
        }

        JSONObject dataJson = resultJson.getJSONObject("data");
        JSONArray dataList = dataJson.getJSONArray("data");
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("商品(只查询电五价格)：").append(dataJson.getStr("name")).append("\n\n");

        JSONArray returnData = new JSONArray();
        for (int i = 0; i < dataList.size(); i++) {
            JSONArray list = dataList.getJSONArray(i);
            if (CollUtil.isEmpty(list)) {
                break;
            }
            if (CollUtil.isNotEmpty(list) && "电信五区".equals(list.getJSONObject(0).getStr("zone"))) {
                returnData = list;
            }
        }

        for (Object o : returnData) {
            JSONObject data = (JSONObject) o;
            stringBuilder.append("区服：").append(data.getStr("server")).append("\n");
            stringBuilder.append("价格：").append(data.getStr("value")).append(data.getStr("sale")).append("\n");
            stringBuilder.append("时间：").append(data.getStr("date")).append("\n");
        }

//        if (CharSequenceUtil.isNotBlank(dataJson.getStr("file")) && CharSequenceUtil.isNotBlank(dataJson.getStr("upload"))) {
//            stringBuilder.append("[CQ:image,file=").append(dataJson.getStr("file"))
//                    .append(",url=").append(dataJson.getStr("upload")).append("]\n");
//        }
//        stringBuilder.append("详情：" + robotwujiaurl).append(URLEncodeUtil.encode(message));


        CqMessageUtil.groupSend(groupId, stringBuilder.toString());
    }


    /**
     * 配装查询
     *
     * @param message
     * @return
     */
    public void Peizhuang(String groupId, String message) {
        message = message.replace(" ", "");
        message = message.replace("配装", "");
        String server = "";
        for (String key : TabsName.hongMap.keySet()) {
            if (message.contains(key)) {
                server = TabsName.hongMap.get(key).toString();
            }
        }

        if (CharSequenceUtil.isEmpty(server)) {
            CqMessageUtil.groupSend(groupId, "这是哪个心法？辣鸡西山居还没出的吧");
            return;
        }

        String result = Jx3ApiHttpUtil.Peizhuang(message);
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "配装查询API出问题了，嘤嘤嘤");
            return;
        }

        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "配装查询API出问题了，嘤嘤嘤");
            return;
        }

        JSONObject data = resultJson.getJSONObject("data");
        String str = "心法：" + data.getStr("name") + "\n" +
                "pve推荐：\n" +
                "[CQ:image,file=" + data.getStr("pve") + ",getStr=" + data.get("pve") + "]\n" +
                "pvp推荐：\n" +
                "[CQ:image,file=" + data.getStr("pvp") + ",getStr=" + data.get("pvp") + "]\n";

        CqMessageUtil.groupSend(groupId, str, "false");
    }

    /**
     * 沙盘
     *
     * @param message
     * @return
     */
    public void shapan(String groupId, String message) {
        message = message.replace(" ", "");
        message = message.replace("沙盘", "");
        String server = "";
        for (String key : TabsName.server.keySet()) {
            if (message.contains(key)) {
                server = TabsName.server.get(key).toString();
            }
        }

        if (CharSequenceUtil.isEmpty(server)) {
            CqMessageUtil.groupSend(groupId, "这是哪个心法？辣鸡西山居还没出的吧");
            return;
        }

        String result = Jx3ApiHttpUtil.shapan(message);
        if (!JSONUtil.isTypeJSON(result)) {
            CqMessageUtil.groupSend(groupId, "沙盘API出问题了，嘤嘤嘤");
            return;
        }

        JSONObject resultJson = JSONUtil.parseObj(result);
        if (resultJson.getInt("code") != 200) {
            CqMessageUtil.groupSend(groupId, "沙盘API出问题了，嘤嘤嘤");
            return;
        }

        JSONArray data = resultJson.getJSONArray("data");
        JSONObject o = data.getJSONObject(0);

        String str = "" + "区服：" + o.get("server") + "\n" +
                "[CQ:image,file=" + o.get("url") + ",url=" + o.get("url") + "]";
        CqMessageUtil.groupSend(groupId, str, "false");
    }

}