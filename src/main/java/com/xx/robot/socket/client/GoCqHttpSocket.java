package com.xx.robot.socket.client;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xx.robot.common.util.SpringContextUtil;
import com.xx.robot.common.util.gocphttp.CqMessageUtil;
import com.xx.robot.common.util.message.PublicMessageUtil;
import com.xx.robot.common.bean.CpHttpConfigBean;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

/**
 * websocket客户端监听类
 *
 * @author 。
 */
@Slf4j
public class GoCqHttpSocket extends WebSocketClient {

    /**
     * 关闭/true 打开链接
     */
    public static boolean WEB_STATUS = false;

    public GoCqHttpSocket() throws URISyntaxException {
        super(new URI(CpHttpConfigBean.WS_TCP));
    }


    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        WEB_STATUS = true;
        log.info("go cqhttp-链接成功");
    }

    @Override
    public void onMessage(String s) {
        //获取消息
        JSONObject messageJson = JSONUtil.parseObj(s);
        //获取消息发送人
        JSONObject sender = messageJson.getJSONObject("sender");
        //指令
        String message = messageJson.getStr("message");

        //我们只处理消息类型数据
        if (CharSequenceUtil.equals("message", messageJson.getStr("post_type"))) {
            log.info(message);

            //群组消息 （这里呢我们主要做群机器人，所以解析群得）
            if (CharSequenceUtil.equals("group", messageJson.getStr("message_type"))) {
                SpringContextUtil.groupMessage.distribute(messageJson, sender);
                return;
            }
            //划分消息类型 （个人消息）
            if (CharSequenceUtil.equals("private", messageJson.getStr("message_type"))) {
                // 发送着指定大号
                if (CharSequenceUtil.equals(CpHttpConfigBean.NO_OPEN, sender.getStr("user_id"))) {
                    // 群发
                    if (message.startsWith("群发")) {
                        CqMessageUtil.groupSendAll(CharSequenceUtil.replace(message, "群发", StrUtil.EMPTY));
                        return;
                    }
                }

            }

            log.info("其他消息类型");
        }
        // request
        if (CharSequenceUtil.equals("request", messageJson.getStr("post_type"))) {
            //获取群id
            String groupId = messageJson.getStr("group_id");
            //获取 flag
            String flag = messageJson.getStr("flag");
            //获取 sub_type
            String subType = messageJson.getStr("sub_type");

            if (CharSequenceUtil.equals("group", messageJson.getStr("request_type"))) {
                String serverName = PublicMessageUtil.bindServer(groupId);
                HashMap<String, String> map = new HashMap<>();
                map.put("flag", flag);
                map.put("sub_type", subType);
                if (CharSequenceUtil.isBlank(serverName)) {
//                    map.put("approve", "true");
//                    map.put("reason", "大家好，我是机器人丐丐，你可以对我说：菜单，查看功能额！");
//                    SpringContextUtil.GoCqHttpUtil.group_request(map);
                } else {
//                    map.put("approve", "false");
//                    map.put("reason", "每个群只能添加一次,一旦放弃使用,便无法再次申请使用。");
//                    SpringContextUtil.GoCqHttpUtil.group_request(map);
                }
            }
            if (CharSequenceUtil.equals("friend", messageJson.getStr("request_type"))) {
                log.info("request_type:friend");
            }
        }

    }

    @Override
    public void onClose(int i, String s, boolean b) {
        log.info("噗连接断开");
        WEB_STATUS = false;
    }

    @Override
    public void onError(Exception e) {
        log.error("链接出错了哟！", e);
    }

}