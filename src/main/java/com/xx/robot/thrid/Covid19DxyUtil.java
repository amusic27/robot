package com.xx.robot.thrid;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 本接口为全国新型肺炎疫情实时数据接口，数据来源为丁香园
 *
 * @author xx
 * @uri https://lab.isaaclin.cn/nCoV/
 */
@Slf4j
public class Covid19DxyUtil {

    private String COVID19_DXY = "Covid19_Dxy";

    private static final String BASE_URL = "https://lab.isaaclin.cn/nCoV/api/";
    private static final String OVERALL_PATH = "overall";
    private static final String PROVINCE_NAME_PATH = "provinceName";
    private static final String AREA_PATH = "area";
    private static final String NEWS_PATH = "news";
    private static final String RUMORS_PATH = "rumors";

    /**
     * 全国疫情概览
     *
     * <p> currentConfirmedCount(Incr)	现存确诊人数（较昨日增加数量）值为 confirmedCount (Incr) - curedCount (Incr) - deadCount (Incr)
     * <p> confirmedCount(Incr)	累计确诊人数（较昨日增加数量）
     * <p> suspectedCount(Incr)	疑似感染人数（较昨日增加数量）
     * <p> curedCount(Incr)	治愈人数（较昨日增加数量）
     * <p> deadCount(Incr)	死亡人数（较昨日增加数量）
     * <p> seriousCount(Incr)	重症病例人数（较昨日增加数量）
     * <p> updateTime	数据最后变动时间
     *
     * @return 返回自爬虫运行开始（2020 年 1 月 24 日下午 4:00）至今，病毒研究情况以及全国疫情概览，可指定返回数据为最新发布数据或时间序列数据
     */
    public static String overall() {
        return HttpUtil.get(BASE_URL + OVERALL_PATH);
    }

    /**
     * 国家、省份、地区、直辖市列表 默认中文
     *
     * @return 返回数据库内有数据条目的国家、省份、地区、直辖市列表。
     */
    public static String provinceName(String lang) {

        String url = BASE_URL + PROVINCE_NAME_PATH;
        if (CharSequenceUtil.equalsIgnoreCase(lang, "en")) {
            url = url + "?lang=en";
        }
        return HttpUtil.get(url, 30000);
    }

    /**
     * 返回自 2020 年 1 月 22 日凌晨 3:00（爬虫开始运行）至今，中国所有省份、地区或直辖市及世界其他国家的所有疫情信息变化的时间序列数据（精确到市），
     * 能够追溯确诊 / 疑似感染 / 治愈 / 死亡人数的时间序列。
     * <p> 自 2020 年 1 月 22 日凌晨 3:00 至 2020 年 1 月 24 日凌晨 3:40 之间的数据只有省级数据，自 2020 年 1 月 24 日起，丁香园才开始统计并公开市级数据。
     *
     * @return 具体地区的疫情数据
     */
    public static String area(String provinceName) {
        String url = BASE_URL + AREA_PATH + "?lang=zh&latest=1&province=" + provinceName;
        return HttpUtil.get(url);
    }

    /**
     * 返回自 2020 年 1 月 22 日凌晨 3:00（爬虫开始运行）至今，中国所有省份、地区或直辖市及世界其他国家的所有疫情信息变化的时间序列数据（精确到市），
     * 能够追溯确诊 / 疑似感染 / 治愈 / 死亡人数的时间序列。
     * <p> 自 2020 年 1 月 22 日凌晨 3:00 至 2020 年 1 月 24 日凌晨 3:40 之间的数据只有省级数据，自 2020 年 1 月 24 日起，丁香园才开始统计并公开市级数据。
     *
     * @return 具体地区的疫情数据
     */
    public static String news(int page, int num) {
        page = ObjectUtil.isNull(page) ? 1 : page;
        num = ObjectUtil.isNull(num) ? 10 : num;

        String url = BASE_URL + NEWS_PATH + "?page=" + page + "&num=" + num;
        return HttpUtil.get(url);
    }

    /**
     * 返回自 2020 年 1 月 22 日凌晨 3:00（爬虫开始运行）至今，中国所有省份、地区或直辖市及世界其他国家的所有疫情信息变化的时间序列数据（精确到市），
     * 能够追溯确诊 / 疑似感染 / 治愈 / 死亡人数的时间序列。
     * <p> 自 2020 年 1 月 22 日凌晨 3:00 至 2020 年 1 月 24 日凌晨 3:40 之间的数据只有省级数据，自 2020 年 1 月 24 日起，丁香园才开始统计并公开市级数据。
     *
     * @return 具体地区的疫情数据
     */
    public static String rumors(int page, int num) {
        page = ObjectUtil.isNull(page) ? 1 : page;
        num = ObjectUtil.isNull(num) ? 10 : num;

        String url = BASE_URL + RUMORS_PATH + "?page=" + page + "&num=" + num;
        return HttpUtil.get(url);
    }

}
