package com.xx.robot.thrid;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotSearchUtil {

    /**
     * 知乎热榜
     */
    private static final String HOT_WEIBO_URL = "https://tenapi.cn/resou/";
    private static final String HOT_BILIBILI_URL = "https://api.bilibili.com/x/web-interface/popular";
    private static final String RANK_BILIBILI_URL = "https://api.bilibili.com/x/web-interface/ranking/v2";


    public static String hotWeibo() {
        return HttpUtil.get(HOT_WEIBO_URL);
    }

    public static JSONArray hotBilibili() {
        JSONArray jsonArray = JSONUtil.createArray();
        String result = HttpUtil.get(HOT_BILIBILI_URL);
        if (CharSequenceUtil.isNotBlank(result)) {
            JSONObject jsonObject = JSONUtil.parseObj(result);
            String code = jsonObject.getStr("code");
            if (CharSequenceUtil.equals(code, "0")) {
                jsonArray = jsonObject.getJSONObject("data").getJSONArray("list");
            }
        }
        return jsonArray;
    }

    public static JSONArray rankBilibili() {
        JSONArray jsonArray = JSONUtil.createArray();
        String result = HttpUtil.get(RANK_BILIBILI_URL);
        if (CharSequenceUtil.isNotBlank(result)) {
            JSONObject jsonObject = JSONUtil.parseObj(result);
            String code = jsonObject.getStr("code");
            if (CharSequenceUtil.equals(code, "0")) {
                jsonArray = jsonObject.getJSONObject("data").getJSONArray("list");
            }
        }
        return jsonArray;
    }

}
