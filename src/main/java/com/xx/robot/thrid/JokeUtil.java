package com.xx.robot.thrid;

import cn.hutool.http.HttpUtil;

/**
 * 段子
 */
public class JokeUtil {

    private static final String SAO_WORDS_URL = "https://api.vvhan.com/api/sao";
    private static final String LOVE_WORDS_URL = "https://api.vvhan.com/api/love";

    /**
     * @return 随机一个骚话。
     */
    public static String saoWords() {
        return HttpUtil.get(SAO_WORDS_URL, 30000);
    }
    /**
     * @return 随机一个情话。
     */
    public static String loveWords() {
        return HttpUtil.get(LOVE_WORDS_URL, 30000);
    }

    /**
     * @return 随机一个情话。
     */
    public static String joke() {

        return HttpUtil.get(LOVE_WORDS_URL, 30000);
    }
}
