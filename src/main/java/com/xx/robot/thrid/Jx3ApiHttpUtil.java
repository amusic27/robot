package com.xx.robot.thrid;

import cn.hutool.http.HttpUtil;
import com.xx.robot.common.bean.CpHttpConfigBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.xx.robot.common.constant.SettingUrl.*;

/**
 * TODO
 *
 * @author Lenovo
 * @Name jx3Api_rc
 */
@Slf4j
@Component
public class Jx3ApiHttpUtil {


    /**
     * 获取默认日常信息
     */
    public static String daily(String serverName) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("server", serverName);
        return HttpUtil.get(jx3Apirichang, paramMap, 3000);
    }


    /**
     * 宏查询
     *
     * @param message
     * @return
     */
    public static String hong(String message) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("name", message);
        return HttpUtil.get(jx3Apihong, paramMap, 3000);
    }


    /**
     * 服务器状态查询
     *
     * @param message 服务器
     * @return
     */
    public static String server(String message) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("server", message);
        return HttpUtil.get(jx3Apiserver, paramMap, 3000);
    }

    /**
     * 服务器金价查询
     *
     * @param serverName 服务器
     * @return
     */
    public static String jinjia(String serverName) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("server", serverName);
        return HttpUtil.get(jx3Apijinjia, paramMap, 3000);
    }

    /**
     * 添狗日记
     *
     * @return
     */
    public static String tiangou() {
        return HttpUtil.get(jx3Apitiangou, 3000);
    }

    /**
     * 骚话
     *
     * @return 骚话
     */
    public static String saoWords() {
        return HttpUtil.get(jx3Apisaohua);
    }

    /**
     * 智能闲聊
     *
     * @param message 消息
     * @return
     */
    public static String message(String message) {
        HashMap<String, Object> parm = new HashMap<>(8);
        parm.put("secretId", TencentsecretId);
        parm.put("secretKey", TencentsecretKey);
        parm.put("name", CpHttpConfigBean.ROBOT_NAME);
        parm.put("question", message);
        return HttpUtil.get(jx3Apixianliao, parm, 3000);
    }

    /**
     * 物价查询
     *
     * @return
     */
    public static String PriceInquiry(String message) {

        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("name", message);
        return HttpUtil.get(jx3Apiwujia, paramMap, 3000);

    }

    /**
     * 配装查询
     *
     * @param serverName
     * @return
     */
    public static String Peizhuang(String serverName) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("name", serverName);
        return HttpUtil.get(jx3Apipeizhuang, paramMap, 3000);
    }

    /**
     * 沙盘
     *
     * @param serverName
     * @return
     */
    public static String shapan(String serverName) {
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("server", serverName);
        return HttpUtil.get(jx3Apishapan, paramMap, 3000);
    }
}
