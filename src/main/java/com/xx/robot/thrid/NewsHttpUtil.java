package com.xx.robot.thrid;

import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 第三方新闻相关
 */
@Slf4j
public class NewsHttpUtil {

    // https://api.vvhan.com/api/60s
    private static final String WORLD_60S_IMG_URL = "https://api.03c3.cn/zb/api.php";
    private static final String WORLD_60S_JSON_URL = "https://api.vvhan.com/api/60s?type=json";
    private static final String FISH_URL = "https://vps.gamehook.top/api/face/my";
    private static final String FISH_IMG_URL = "https://api.vvhan.com/api/moyu?type=json";

    /**
     * 摸鱼提醒
     *
     * @return 提醒文案
     */
    public static String world60sImg() {
        return HttpUtil.get(WORLD_60S_IMG_URL, 30000);
    }

    /**
     * 60s新闻
     *
     * @return 新闻
     */
    public static String world60sJson() {
        return HttpUtil.get(WORLD_60S_JSON_URL, 30000);
    }

    /**
     * 摸鱼提醒
     *
     * @return 提醒文案
     */
    public static String fishRemind() {
        return HttpUtil.get(FISH_URL, 30000);
    }

    /**
     * 摸鱼提醒
     *
     * @return 提醒文案
     */
    public static String fishRemindImg() {
        return HttpUtil.get(FISH_IMG_URL, 30000);
    }

}
