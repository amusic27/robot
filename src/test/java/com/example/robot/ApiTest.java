package com.example.robot;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.xx.robot.common.util.mongo.MongoDbUtil;
import com.xx.robot.common.constant.CollectionNameEnum;
import com.xx.robot.common.constant.Jx3BoxUrlEnum;
import com.xx.robot.thrid.Jx3ApiHttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.HashMap;
import java.util.List;

@Slf4j
public class ApiTest {


    @Test
    public void apiServer() {
        String results = HttpUtil.get("https://spider.jx3box.com/jx3servers");
        if (!JSONUtil.isJson(results)) {
            log.error("失败");
        }
        JSONObject resultJson = JSONUtil.parseObj(results);
        JSONArray serverList = resultJson.getJSONArray("data");

        List<JSONObject> groups = MongoDbUtil.selectAll(JSONObject.class, CollectionNameEnum.JX3_SERVER.getCode());
        for (JSONObject group : groups) {
            String serverNames = group.getStr("server");
            String groupId = group.getStr("_id");
            String connectState = group.getStr("connectState");

            // 查看状态
            String status = serverList.stream()
                    .filter(o -> StrUtil.equals(((JSONObject) o).getStr("mainServer"), serverNames))
                    .map(p -> ((JSONObject) p).getStr("connectState"))
                    .findFirst().orElse("没找到");

            if (!connectState.equals(status)) {
                log.info("{}服务器现在:{}", serverNames, status);

                log.info("修改mongo为:{}", status);
                Query query = new Query(Criteria.where("_id").is(groupId));
                Update update = new Update().set("serverStatus", status);
                MongoDbUtil.updateFirst(query, update, CollectionNameEnum.JX3_SERVER.getCode());
            }
        }
    }

    @Test
    public void ip() {

        HashMap<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("kungfu", "10003");
        paramMap.put("size", "1");
        paramMap.put("client", "std");
        String result = HttpUtil.get("https://next2.jx3box.com/api/macro/tops", paramMap);
        log.info(result);
        String pid = JSONUtil.parseArray(result).getJSONObject(0).getStr("pid");
        log.info(pid);

        String micro = HttpUtil.get("https://cms.jx3box.com/api/cms/post/" + pid);
        log.info(micro);
        JSONObject microJson = JSONUtil.parseObj(micro);

        JSONObject data = microJson.getJSONObject("data");
        String author = data.getStr("author");

        StringBuilder str = new StringBuilder();
        JSONArray macroList = data.getJSONObject("post_meta").getJSONArray("data");

        // 最多查询3条
        int len = Math.min(macroList.size(), 3);
        for (int i = 0; i < len; i++) {
            JSONObject o = macroList.getJSONObject(i);
            str.append("宏名称：").append(author).append("#").append(o.getStr("name")).append("\n");
            str.append("备注：【").append(o.getStr("desc")).append("】\n");
            str.append("奇穴：【").append(o.getStr("talent")).append("】\n");
            str.append(o.getStr("macro")).append("\n\n");
        }
        log.info(str.toString());

    }

    @Test
    void testApi() {
        String results = Jx3ApiHttpUtil.daily("梦江南");
        log.info(JSONUtil.toJsonPrettyStr(results));
    }

    @Test
    void testServerStatus() {
        String result = HttpUtil.get(Jx3BoxUrlEnum.JX3BOX_SERVER_URL);

        if (!JSONUtil.isTypeJSON(result)) {
            log.error("api出问题了。。");
        }

        JSONObject resultJson = JSONUtil.parseObj(result);
        JSONArray serverStatusList = resultJson.getJSONArray("data");

        List<JSONObject> serverStatus2 = serverStatusList.stream()
                .map(x -> (JSONObject) x)
                .filter(o -> o.getStr("serverName").equals("梦江南")).toList();
        log.info(serverStatus2.get(0).getStr("connectState"));
    }


    @Test
    public void testMac() {
        HashMap<String, Object> paramMap = new HashMap<>(8);
        paramMap.put("type", "macro");
        paramMap.put("per", "10");
        paramMap.put("page", "1");
        paramMap.put("subtype", URLUtil.decode("易筋经"));
        paramMap.put("order", "update");
        paramMap.put("client", "std");
        paramMap.put("sticky", "1");

        log.info(HttpUtil.get("https://cms.jx3box.com/api/cms/posts", paramMap));
    }
}
